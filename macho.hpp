#ifndef MACHO_HPP
#define MACHO_HPP

#include "util.hpp"

#include <mach/machine.h>
#include <string>
#include <unordered_map>

using SymbolMap = std::unordered_map<std::string, uint64_t>;

class MachO {
public:
	MachO(void* machOBaseAddress);

	void BuildSymbolMap();
	template<typename T = uint64_t> T QuerySymbol(std::string symbol) {
		if(this->symbolMap.count(symbol)) {
			return static_cast<T>(this->symbolMap.find(symbol)->second);
		}
		throw std::runtime_error(Format("Cannot find symbol '%s'", symbol.c_str()));
		return 0;
	}

	template<typename T = void*> T GetMachOBaseAddress() {
		return reinterpret_cast<T>(this->machOBaseAddress);
	}

private:
	void* machOBaseAddress;
	SymbolMap symbolMap;
};

#endif // MACHO_HPP
