#ifndef MACHOFILE_HPP
#define MACHOFILE_HPP

#include "macho.hpp"

#include <cstddef>
#include <mach/machine.h>

class MachOFile {
public:
	MachOFile(const char* filePath);
	~MachOFile();

	MachO* macho;

private:
	int fileDescriptor;
	void* mappedBase;
	size_t mappedSize;
};

#endif // MACHOFILE_HPP
