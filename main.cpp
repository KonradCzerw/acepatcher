#include "machofile.hpp"

#include <exception>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char** argv) {
	try {
		char* discordVoicePath = nullptr;
		int c;
		while((c = getopt(argc, argv, "hd:")) != -1) {
			switch(c) {
			case 'h':
				printf("[I] Usage:\n");
				printf("[I] %s -d <path to discord_voice.node>\n", argv[0]);
				break;
			case 'd':
				discordVoicePath = optarg;
				break;
			}
		}

		if(discordVoicePath == nullptr) {
			printf("[E] discord_voice.node path not provided!\n");
		}

		MachOFile discordVoice(discordVoicePath);
		discordVoice.macho->BuildSymbolMap();
		auto baseAddress = discordVoice.macho->GetMachOBaseAddress<uintptr_t>();

		auto jzSearch = baseAddress + discordVoice.macho->QuerySymbol<intptr_t>("__ZN7discord5media10soundshare19GetAceInstallStatusEv");
		printf("[I] GetAceInstallStatus @ 0x%lx\n", jzSearch);

		bool patched = false;
		for(intptr_t offset = 0; offset < 0x40; offset++) {
			if(*reinterpret_cast<uint16_t*>(jzSearch) == 0x840F) {
				patched = true;
				printf("[I] jz @ 0x%lx - 0x%lx\n", offset, jzSearch);
				*reinterpret_cast<uint16_t*>(jzSearch) = 0x04EB;
				break;
			}
			jzSearch++;
		}

		if(patched) {
			printf("[I] Patched successfully!\n");
		} else {
			printf("[E] Patch failed! (your discord_voice.node may already be patched)\n");
			return 1;
		}
	} catch(std::exception& ex) {
		printf("[E] %s\n", ex.what());
		return 1;
	}
	return 0;
}
