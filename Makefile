TARGET := acepatcher

CXX := clang++
CXXFLAGS := -std=c++17 -Wall -Wextra -Wpedantic
LDFLAGS := 

SRCS := $(wildcard *.cpp)
OBJS := $(SRCS:.cpp=.o)
DEPS := $(SRCS:.cpp=.d)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^

-include $(DEPS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -MMD -MP -c -o $@ $<

clean:
	rm -f $(TARGET) $(OBJS) $(DEPS)