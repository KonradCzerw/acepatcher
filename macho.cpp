#include "macho.hpp"

#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include <mach-o/fat.h>
#include <stdio.h>
#include <bit>

class LoadCommandWrapper {
public:
	LoadCommandWrapper(void* machOBaseAddress) {
		auto machOHeader = reinterpret_cast<mach_header_64*>(machOBaseAddress);
		if(machOHeader->magic != MH_MAGIC_64) throw std::runtime_error("Invalid MachO magic");
		this->loadCommands = reinterpret_cast<load_command*>(reinterpret_cast<uintptr_t>(machOBaseAddress) + sizeof(mach_header_64));
		this->loadCommandSize = machOHeader->sizeofcmds;
	}

	class LoadCommandIterator {
	public:
		LoadCommandIterator(load_command* loadCommand) : loadCommand(loadCommand) { }

		load_command* operator*() { return this->loadCommand; };
		LoadCommandIterator& operator++() {
			this->loadCommand = reinterpret_cast<load_command*>(reinterpret_cast<uintptr_t>(this->loadCommand) + this->loadCommand->cmdsize);
			return *this;
		}
		bool operator!=(const LoadCommandIterator& other) { return this->loadCommand != other.loadCommand; }

	private:
		load_command* loadCommand;
	};

	LoadCommandIterator begin() {
		return LoadCommandIterator(this->loadCommands);
	}

	LoadCommandIterator end() {
		return LoadCommandIterator(reinterpret_cast<load_command*>(reinterpret_cast<uintptr_t>(this->loadCommands) + this->loadCommandSize));
	}

private:
	load_command* loadCommands;
	size_t loadCommandSize;
};

void* UnwrapFatBinary(void* fatMachOBaseAddress) {
	auto header = reinterpret_cast<fat_header*>(fatMachOBaseAddress);
	for(auto architectrue : ArrayView<fat_arch>(
		reinterpret_cast<fat_arch*>(reinterpret_cast<uintptr_t>(fatMachOBaseAddress) + sizeof(fat_header)),
		FastConvertEndian(header->nfat_arch)
	)) {
		if(FastConvertEndian(architectrue.cputype) == CPU_TYPE_X86_64) {
			return reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(fatMachOBaseAddress) + FastConvertEndian(architectrue.offset));
		}
	}
	throw std::runtime_error("Cannot find architecture in fatMachO");
	return nullptr;
}

MachO::MachO(void* machOBaseAddress) {
	if(machOBaseAddress == nullptr) throw std::runtime_error("Invalid MachO baseAddress");
	
	switch(*reinterpret_cast<uint32_t*>(machOBaseAddress)) {
	case FAT_CIGAM:
		this->machOBaseAddress = UnwrapFatBinary(machOBaseAddress);
		break;
	case MH_MAGIC_64:
		this->machOBaseAddress = machOBaseAddress;
		break;
	default:
		throw std::runtime_error("Invalid MachO magic");
	}
}

void MachO::BuildSymbolMap() {
	for(auto cmd : LoadCommandWrapper(this->machOBaseAddress)) {
		if(cmd->cmd == LC_SYMTAB) {
			auto symtabCommand = reinterpret_cast<symtab_command*>(cmd);
			auto stringTable = reinterpret_cast<uintptr_t>(this->machOBaseAddress) + symtabCommand->stroff;

			for(auto symbol : ArrayView<nlist_64>(
				reinterpret_cast<nlist_64*>(reinterpret_cast<uintptr_t>(this->machOBaseAddress) + symtabCommand->symoff),
				symtabCommand->nsyms
			)) {
				this->symbolMap.insert({
					reinterpret_cast<char*>(stringTable + symbol.n_un.n_strx),
					symbol.n_value
				});
			}
		}
	}
}


