#!/bin/sh

VOICE="`find "${HOME}/Library/Application Support/discord" -type d -name '0.0*' -print -quit`/modules/discord_voice"

cp -r binaries/* "${VOICE}"
./acepatcher -d "${VOICE}/discord_voice.node"
