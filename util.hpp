#ifndef UTIL_HPP
#define UTIL_HPP

#include <string>
#include <memory>

template<typename... Args> std::string Format(const char* format, Args... args) {
	int size = snprintf(nullptr, 0, format, args...) + 1;
	if(size == 0) throw std::runtime_error("Failed to format string");
	std::unique_ptr<char[]> buffer(new char[size]);
	snprintf(buffer.get(), size, format, args...);
	return std::string(buffer.get(), buffer.get() + size - 1);
}

template<typename T> class ArrayView {
public:
	ArrayView(T* arrayPointer, size_t arrayLength) : arrayPointer(arrayPointer), arrayLength(arrayLength) {}

	T& operator[](size_t index) {
		return this->arrayPointer[index];
	}

	const T& operator[](size_t index) const {
		return this->arrayPointer[index];
	}

	T* begin() {
		return this->arrayPointer;
	}

	const T* begin() const {
		return this->arrayPointer;
	}

	T* end() {
		return this->arrayPointer + this->arrayLength;
	}

	const T* end() const {
		return this->arrayPointer + this->arrayLength;
	}

private:
	T* arrayPointer;
	size_t arrayLength;
};

template<typename T> T FastConvertEndian(T value) {
	if constexpr(sizeof(T) == 2) {
		asm("sar %0, 8\n\t" : "+r" (value));
		return value;
	} else if constexpr(sizeof(T) == 4 || sizeof(T) == 8) {
		asm("bswap %0\n\t" : "+r" (value));
		return value;
	} else {
		static_assert(sizeof(T) == 2 || sizeof(T) == 4 || sizeof(T) == 8, "Unsupported value size");
		return value;
	}
}

#endif // UTIL_HPP
