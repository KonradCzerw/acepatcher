#include "machofile.hpp"

#include "util.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <stdexcept>

MachOFile::MachOFile(const char* filePath) {
	this->fileDescriptor = open(filePath, O_RDWR);
	if(this->fileDescriptor == -1) throw std::runtime_error(Format("Failed to open file '%s' (%s)", filePath, strerror(errno)));

	struct stat st;
	if(fstat(this->fileDescriptor, &st) == -1) throw std::runtime_error(Format("Failed to stat file '%s' (%s)", filePath, strerror(errno)));
	this->mappedSize = st.st_size;

	this->mappedBase = mmap(nullptr, this->mappedSize, PROT_WRITE | PROT_READ, MAP_SHARED, this->fileDescriptor, 0);
	if(this->mappedBase == MAP_FAILED) throw std::runtime_error(Format("Failed to map file '%s' (%s)", filePath, strerror(errno)));

	this->macho = new MachO(this->mappedBase);
}

MachOFile::~MachOFile() {
	if(this->macho) delete this->macho;

	if(this->mappedBase != MAP_FAILED) {
		msync(this->mappedBase, this->mappedSize, MS_SYNC);
		munmap(this->mappedBase, this->mappedSize);
	}

	if(this->fileDescriptor != -1) close(this->fileDescriptor);
}
